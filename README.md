Website
=======
http://www.libsdl.org/projects/SDL_mixer/

License
=======
zlib license (see the file source/COPYING)

Version
=======
1.2.x

Source
======
https://hg.libsdl.org/SDL_mixer/ (branch: SDL-1.2, changeset: 597:a4e9c53d9c30)

Requires
========
* Ogg
* SDL1
* Vorbis